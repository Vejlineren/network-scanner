import scapy.all as scapy
import argparse

def scan(ip):
    arp_request = scapy.ARP(pdst=ip) # Create ARP request and specify search ip range
    broadcast = scapy.Ether(dst="ff:ff:ff:ff:ff:ff") # Create ethernet object

    # Create final packet
    arp_request_broadcast = broadcast/arp_request # Combine the two packets

    # Send packet
    answered_list = scapy.srp(arp_request_broadcast, timeout=1,verbose=False)[0] # Send/response with custom Ether packet. Capture only answered packets.
    
    # Structure result
    result = {}
    for element in answered_list:
        result[element[1].psrc] = element[1].hwsrc

    return result


def show_result(result):
    # Print scanning result
    print("IP\t\t\tMAC\n------------------------------------------------------------------")

    for ip,mac in result.items():
        print(ip + "\t\t" + mac)


result = scan("10.0.2.1/24")
show_result(result)